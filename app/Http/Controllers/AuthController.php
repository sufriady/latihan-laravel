<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function auth()
   {
       return view('page.register');
   }

   public function kirim(Request $request)
   {
      // dd($request->all());
      $firstname = $request['first_name'];
      $lastname = $request['last_name'];

      return view('page.welcome', compact("firstname","lastname"));
        
   }
}
