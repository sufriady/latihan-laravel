@extends('layout.master')
@section('title')
Buat Account Baru
@endsection
@section('content')
    <h3>Sign up form</h3>
    
    <form action="/kirim" method="POST">
        @csrf
        <label>Fist Name</label><br>
        <input type="text" name="first_name"><br><br>
        <label>Last Name</label><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender</label><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <label >Nationality</label><br>
        <select name="Nationality" id="">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Other</option>
        </select><br><br>
        <label">Languange Spoken</label><br>
        <input type="checkbox" name="languange">Bahasa Indonesia <br>
        <input type="checkbox" name="languange">English <br>
        <input type="checkbox" name="languange">Other <br><br>
        <label">Bio</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>


        <input type="submit" value="Sign Up">     

    </form>


@endsection